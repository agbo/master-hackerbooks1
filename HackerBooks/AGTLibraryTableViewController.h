//
//  AGTLibraryTableViewController.h
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 13/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;

@class AGTLibrary;
@class AGTLibraryTableViewController;
@class AGTBook;

@protocol AGTLibraryTableViewControllerDelegate <NSObject>

@optional
-(void) libraryTableViewController:(AGTLibraryTableViewController*) vc
                     didSelectBook:(AGTBook*)newBook;

@end

@interface AGTLibraryTableViewController : UITableViewController <AGTLibraryTableViewControllerDelegate>

@property (weak, nonatomic) id<AGTLibraryTableViewControllerDelegate> delegate;

-(id) initWithModel:(AGTLibrary*) library
              style:(UITableViewStyle) style;

@end
