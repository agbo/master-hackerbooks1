//
//  AGTSimplePDFViewController.h
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 17/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;

@class AGTBook;

@interface AGTSimplePDFViewController : UIViewController

@property(nonatomic, strong) AGTBook *model;
@property (weak, nonatomic) IBOutlet UIWebView *pdfView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;

-(id)initWithModel:(AGTBook*) book;

@end
