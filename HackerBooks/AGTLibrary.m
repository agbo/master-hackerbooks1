//
//  AGTLibrary.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 10/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTLibrary.h"
#import "AGTBook.h"
#import "AGTMultiDictionary.h"
#import "AGTBookTag.h"

@interface AGTLibrary ()
/*Arrays of books indexed by tags */
@property (nonatomic, strong) AGTMultiDictionary *bookDB;
@end

@implementation AGTLibrary

#pragma mark - Properties
-(NSArray *)tags{
    return [[self.bookDB allKeys] sortedArrayUsingSelector:@selector(compare:)];
}
-(NSString *) title{
    return @"Hacker Books";
}

#pragma mark -  Init
-(id) initWithJSONData:(NSData*) data{
    
    
    if( self = [super init]){
        
        NSError *err;
        id json = [NSJSONSerialization JSONObjectWithData:data
                                                  options:0
                                                    error:&err ];
        if (json == nil) {
            // There was an error
            NSLog(@"Error while parsing json data.\n%@", err);
            self = nil;
        }else if ([json isKindOfClass:[NSArray class]]){
            _bookDB = [AGTMultiDictionary dictionary];
            [self processJSONArray: json];
            [self setupNotifications];
            
            
        }else{
            // Should have been an NSArray
            self = nil;
            
        }
        
    }
    
    return self;
}

-(void) dealloc{
    [self tearDownNotifications];
}

#pragma mark - Accessors

-(NSUInteger) bookCountForTag:(NSString *) tagString{
    return [[self booksForTag:tagString] count];
}


-(NSArray *) booksForTag: (NSString *) tagString{
    
    AGTBookTag *tag = [AGTBookTag bookTagWithName:tagString];
    NSSet *books = [self.bookDB objectsForKey:tag];
    if ([books count]) {
        return [books allObjects];
    }else{
        return nil;
    }
    
}


-(AGTBook*) bookForTag:(NSString*) tagString atIndex:(NSUInteger) index{
    return [[self booksForTag:tagString] objectAtIndex:index];
}

#pragma mark - JSON Processing
-(void) processJSONArray:(NSArray*)json{
    
    
    for (NSDictionary *dict in json) {
        
        AGTBook *current = [[AGTBook alloc] initWithJSONDictionary:dict];
        [self addBook: current];
        _bookCount++;
    }
}


-(void) addBook:(AGTBook*) book{
    
    for (NSString *tag in book.tags) {
        
        [self.bookDB addObject:book
                       forKey:tag];
    }
}


#pragma mark - Notifications
-(void) setupNotifications{
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(notifyThatFavoritesDidChange:)
               name:AGTBOOK_DID_CHANGE_NOTIFICATION
             object:nil];
    
}

-(void) tearDownNotifications{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) notifyThatFavoritesDidChange:(NSNotification *) notification{
    
    // Saco el libro
    AGTBook *book = [notification.userInfo objectForKey:BOOK_KEY];
    
    AGTBookTag *fav = [AGTBookTag favoriteBookTag];
    
    if (book.isFavorite) {
        // Añado
        [self.bookDB addObject:book
                        forKey:fav];
    }else{
        // Lo elimino
        [self.bookDB removeObject:book
                           forKey:fav];
    }
    
    [self postTagChangeNotification];
}

-(void) postTagChangeNotification{
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc postNotificationName:AGTLIBRARY_TAGS_DID_CHANGE_NOTIFICATION
                      object:self];
}


@end
