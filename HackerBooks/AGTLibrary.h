//
//  AGTLibrary.h
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 10/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#define AGTLIBRARY_TAGS_DID_CHANGE_NOTIFICATION @"AGTLibrary_Tags_Changed"

@import Foundation;

@class AGTBook;

@interface AGTLibrary : NSObject

/* Total number of books */
@property (nonatomic, readonly) NSUInteger bookCount;

/** Sorted Array of tags (Favorites is the first tag, no matter lexycographical
 order */
@property (nonatomic, readonly) NSArray *tags;

/** Designated initializer
 */

-(id) initWithJSONData:(NSData*) data;


/* Number of books in a tag. If a tag doesn't exist, should return 0 */
-(NSUInteger) bookCountForTag:(NSString *) tagString;

/**
 Inmutable array of books in a certain tag.
 A book can be in several tags. If there are no books for a certain
 tag, should return nil.
 */
-(NSArray *) booksForTag: (NSString *) tagString;


/**
 Book at the index position within the tag. If either the index or tag 
 doesn't exist, should return nil.
 */
-(AGTBook*) bookForTag:(NSString*) tagString atIndex:(NSUInteger) index;


@end
