//
//  AGTBook.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 10/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTBook.h"
#import "AGTBookTag.h"
#import "AGTAsyncImage.h"

/* JSON Properties */
#define TITLE @"title"
#define AUTHORS @"authors"
#define IMAGE_URL @"image_url"
#define PDF_URL @"pdf_url"
#define TAGS @"tags"

@interface AGTBook ()<AGTAsyncImageDelegate>
@property (nonatomic, strong) AGTAsyncImage *asyncImage;
@end

@implementation AGTBook

#pragma mark - Properties

/**
 La propiedad coverImage no será más que un "envoltorio" de la 
 propiedad image de AGTAsyncImage
 */
-(UIImage*) coverImage{
    return self.asyncImage.image;
}

/** Si un libro es favorito, quiere decir que tiene el fag Favorite. Además,
 sabemos que será siempre el primero del array de tags
 */

-(void)setIsFavorite:(BOOL)isFavorite{
    
    BOOL hasFav = [self hasFavoriteTag];
    
    if (isFavorite) {
        /* Insertamos si hace falta */
        if (!hasFav) {
            [self insertFavoriteTag];
        }
    }else{
        /* quitamos el tag favorito se hace falta */
        if (hasFav) {
            [self removeFavoriteTag];
        }
    }
    
}

-(BOOL) isFavorite{
    return [self hasFavoriteTag];
}

-(id) initWithTitle:(NSString*) title
            authors:(NSArray*) authors
               tags:(NSArray*) tagNames
             pdfURL:(NSURL*) pdfURL
           coverURL:(NSURL*) imageURL{
    
    // Should you check if any of the params are nil?
    // NO. Providing correct JSON is the REST API's job,
    // and it should be tested there and not on every single
    // client: Do Not Repeat Yourself.
    if (self = [super init]) {
        _tags = [self arrayOfTags:tagNames];
        _title = title;
        _authors = authors;
        _pdfURL = pdfURL;
        _coverURL = imageURL;
        _asyncImage = [AGTAsyncImage asyncImageWithURL:imageURL
                                          defaultImage:[UIImage imageNamed:@"emptyBookCover.png"]];
        _asyncImage.delegate = self;
    }
    
    return self;
}

-(id) initWithJSONDictionary:(NSDictionary*) dict{
    
    
    // Must tokenize authors and tags
    
    return [self initWithTitle:[dict objectForKey:TITLE]
                       authors:[self tokenize:[dict objectForKey:AUTHORS]]
                          tags:[self tokenize:[dict objectForKey:TAGS]]
                        pdfURL:[NSURL URLWithString:[dict objectForKey:PDF_URL]]
                      coverURL:[NSURL URLWithString:[dict objectForKey:IMAGE_URL]]];
    
}


#pragma mark -  Utils
-(NSArray*) tokenize:(NSString*) commaSeparatedString{
    
    NSArray *tokens = [commaSeparatedString componentsSeparatedByString:@","];
    NSMutableArray *clean = [NSMutableArray arrayWithCapacity:tokens.count];
    
    for (NSString *token in tokens) {
        [clean addObject:[token stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    }
    
    return clean;
}

-(NSArray *) arrayOfTags:(NSArray *) arrayOfStrings{
    
    NSMutableArray *tags = [NSMutableArray arrayWithCapacity:arrayOfStrings.count];
    
    for (NSString *token in arrayOfStrings) {
        [tags addObject:[AGTBookTag bookTagWithName:token]];
    }
         
    return [tags sortedArrayUsingSelector:@selector(compare:)];
}


-(BOOL) hasFavoriteTag{
    return [[self.tags firstObject] isFavorite];
}

-(void) insertFavoriteTag{
    if (![self hasFavoriteTag]) {
     
        NSMutableArray *mTags = [[self tags] mutableCopy];
        [mTags insertObject:[AGTBookTag favoriteBookTag]
                    atIndex:0];
        self.tags = [NSArray arrayWithArray:mTags];
        
        // Avisamos de cambios
        [self notifyChanges];
    }
}

-(void) removeFavoriteTag{
    /* Sabemos que el 1º es el favorito */
    if ([self hasFavoriteTag]) {
        
        NSMutableArray *mTags = [[self tags] mutableCopy];
        [mTags removeObjectAtIndex:0];
        
        self.tags = [NSArray arrayWithArray:mTags];
        
        [self notifyChanges];
    }
}

/** Avisa (por varios mecanismos) que el libro ha suffrido algún
 cambio. Podríamos ser algo más específicos e indicar qué propiedad 
 ha cambiado, pero en este caso no indispensable (tampoco son tantas
 ni es costoso acceder a ellas.
 Podríamos pasar en el user info un array de keys que han cambiado.
 */
-(void) notifyChanges{
    
    NSNotification *n = [NSNotification
                         notificationWithName:AGTBOOK_DID_CHANGE_NOTIFICATION
                         object:self
                         userInfo:@{BOOK_KEY : self}];
    
    [[NSNotificationCenter defaultCenter] postNotification:n];
    
        
    
}
#pragma mark - NSObject
-(NSString *) description{
    return [NSString stringWithFormat:@"<%@:\nTitle:%@\nAuthors:%@\nTags:%@>",
            [self class], _title, _authors, _tags];
}



#pragma mark - "Cosmetic" stuff
-(NSString *) formattedListOfAuthors{
    
    // Standarize case, sort, join with comma
    NSArray *sorted = [self.authors sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    NSString *concat = [sorted componentsJoinedByString:@", "];
    return [concat capitalizedString];


}
-(NSString *) formattedListOfTagNames{
    
    /**
     Todo buen lenguaje debería de eliminar la necesidad de bucles
     explícitos: son un rollo y fuente común de errores.
     Además de la fast iteration y los bloques, también hay otra
     técnica muy potente llamada KVC Collection Operators.
     En este caso estamos creando un nuevo array con el valor
     de la "clave" normalizedName de cada elemento del antiguo
     array: una linea de código.
     Para más info:
     http://nshipster.com/kvc-collection-operators/
     */
    NSArray *names = [self.tags valueForKeyPath:@"normalizedName"];
    return  [names componentsJoinedByString:@", "];
    
}


#pragma mark - AGTAsyncImageDelegate
-(void)asyncImageDidChange:(AGTAsyncImage *)aImage{
    
    // cuando alguien acceda a mi propiedad coverImage,
    // recibirá la nueva. Eso sí, aviso al mundo
    // que he cambiado.
    
    [self notifyChanges];
}






@end
