//
//  AppDelegate.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 10/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AppDelegate.h"
#import "AGTLibrary.h"
#import "AGTLibraryTableViewController.h"
#import "Settings.h"
#import "AGTAsyncImage.h"
#import "AGTBookViewController.h"
#import "AGTBookTag.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Ajusto el comportamiento de imágenes asíncronas
    if (ZAP_ASYNC_IMAGE_CACHE) {
        [AGTAsyncImage flushLocalCache];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    /** Esto tarda. 
     Lo arraglamos más tarde
     */
    
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://t.co/K9ziV0z3SJ"]];
    
    // creo el modelo
    AGTLibrary *model = [[AGTLibrary alloc] initWithJSONData:data];
    
    
    // Configuro para iPad o iPhone
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // Configuro para iPad
        [self configureForiPadWithModel: model];
    }else{
        // configuro para iPhone
        [self configureForiPhoneWithModel: model];
    }
    
    // Configuro aspecto
    [self configureAppearance];
    
    [self.window makeKeyAndVisible];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark -  Util
-(void) configureForiPadWithModel:(AGTLibrary *)model{
    
    // Creo los controladores
    AGTLibraryTableViewController *lVC = [[AGTLibraryTableViewController alloc]
                                          initWithModel:model
                                          style:UITableViewStylePlain];
    NSString *firstTagName = [[[model tags] firstObject]normalizedName];
    AGTBookViewController *bookVC = [[AGTBookViewController alloc]
                                     initWithModel:[model bookForTag:firstTagName
                                                             atIndex:0]];
    
    // Creo los navigations
    UINavigationController *navLib = [[UINavigationController alloc] initWithRootViewController:lVC];
    UINavigationController *navBook = [[UINavigationController alloc] initWithRootViewController:bookVC];
    
    // Creo el SplitView
    UISplitViewController *splitVC = [UISplitViewController new];
    splitVC.viewControllers = @[navLib, navBook];
    
    // Asigno delegados
    lVC.delegate = bookVC;
    
    // Lo muestro
    self.window.rootViewController = splitVC;

}

-(void) configureForiPhoneWithModel:(AGTLibrary *)model{
    
    // Creo los controladores
    AGTLibraryTableViewController *lVC = [[AGTLibraryTableViewController alloc]
                                          initWithModel:model
                                          style:UITableViewStylePlain];
    
    // Creo los navigations
    UINavigationController *navLib = [[UINavigationController alloc] initWithRootViewController:lVC];
    
    
    // Asigno delegados
    lVC.delegate = lVC;
    
    // Lo muestro
    self.window.rootViewController = navLib;
    
}

-(void) configureAppearance{
    
    // barTintColor cambia el color de la barra
    [[UINavigationBar appearance] setBarTintColor: [UIColor colorWithWhite:0.6
                                                                     alpha:1]];
    // TintColor asigna el color de los botones
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithWhite:0.9
                                                                 alpha:1]];
    
    // La Fuente
    NSShadow * shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor lightGrayColor];
    shadow.shadowOffset = CGSizeMake(1, 1);
    
    [self dumpAllFonts];
    
    // Ojo que es vital acertar con el nombre exatco
    // de la fuente y conseguir eso es un rollo
    // El método dumpFonts te permite ver todas
    // las fuentes isntaladas y sus nombres REALES
    NSLog(@"%@", [self dumpAllFonts]);
    
    UIFont *navbarFont = [UIFont fontWithName:@"HelveticaNeue-CondensedBlack"
                                size:24];
    
    NSDictionary * navBarTitleTextAttributes =
  @{ NSForegroundColorAttributeName : [UIColor colorWithWhite:0.1
                                                        alpha:1],
     NSShadowAttributeName          : shadow,
     NSFontAttributeName            : navbarFont };
    
    [[UINavigationBar appearance] setTitleTextAttributes:navBarTitleTextAttributes];
    
    
    // Barra de botones
    [[UIToolbar appearance] setBarTintColor: [UIColor colorWithWhite:0.6
                                                              alpha:1]];
    
    // TintColor asigna el color de los botones
    [[UIToolbar appearance] setTintColor:[UIColor colorWithWhite:0.9
                                                                 alpha:1]];
    
    // Ahora barra de botonoes y los botones en sí
    UIFont *toolbarFont = [UIFont fontWithName:@"HelveticaNeue-Bold"
                                size:18];
    
    NSDictionary * toolBarTitleTextAttributes =
    @{ NSForegroundColorAttributeName : [UIColor colorWithWhite:0.9
                                                          alpha:1],
       NSFontAttributeName            : toolbarFont };
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:toolBarTitleTextAttributes
                                                forState:UIControlStateNormal];
}

-(NSString *)  dumpAllFonts {
    NSMutableString *fonts = [@"" mutableCopy];
    NSArray *sortedFamilyNames = [[UIFont familyNames]sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
    for (NSString *familyName in sortedFamilyNames) {
        [fonts appendFormat:@"\n\n%@\n============================\n\n", [familyName capitalizedString]];
        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
            [fonts appendFormat:@"·\t%@\n", fontName];
        }
    }
    
    return fonts;
}
@end
