//
//  AGTBookTag.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 12/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#define FAVORITE @"Favorite"


#import "AGTBookTag.h"

@interface AGTBookTag()

@property (nonatomic, copy) NSString *name;
@end

@implementation AGTBookTag

#pragma mark - Properties
-(BOOL)isFavorite{
    return [_name isEqualToString:FAVORITE];
}

#pragma mark - Class Methods
+(instancetype) bookTagWithName:(NSString*)tagName{
    return [[self alloc] initWithName:tagName];
}

+(instancetype) favoriteBookTag{
    return [self bookTagWithName:FAVORITE];
}

#pragma mark - Init
-(id) initWithName:(NSString*) tagName{
    
    if (self = [super init]) {
        _name = [self normalizeCase:tagName];
    }
    return self;
}


-(NSString*) normalizedName{
    return self.name;
}

#pragma mark - Utils
-(NSString*) normalizeCase:(NSString*) aString{
    
    NSString *norm;
    
    if (aString.length <= 1) {
        norm = [aString capitalizedString];
    } else {
        norm = [NSString stringWithFormat:@"%@%@",[[aString substringToIndex:1] uppercaseString],[[aString substringFromIndex:1]lowercaseString]];
    }
    return norm;
}

#pragma mark - Comparison
- (NSComparisonResult)compare:(AGTBookTag *)other{
    
    /* favorite always comes first */
    static NSString *fav = @"Favorite";
    
    if ([[self normalizedName] isEqualToString:[other normalizedName]]) {
        return NSOrderedSame;
    }else if ([[self normalizedName] isEqualToString:fav]){
        return NSOrderedAscending;
    }else if ([[other normalizedName] isEqualToString:fav]){
        return NSOrderedDescending;
    }else{
        /* Compare the rest as strings */
        return [self.name compare:other.normalizedName];
    }
}

#pragma mark - NSCopying
/** Every class that might be used as a key in a Dict must implement NSCopying
 as keys are always copied.
 
 Since bookTags are immutable, we just return self: no real copying.
 */
- (id)copyWithZone:(NSZone *)zone{
    return self;
}


#pragma mark - NSObject
-(NSString*) description{
    return [NSString stringWithFormat:@"<%@: %@>",
            [self class], [self normalizedName]];
}

-(BOOL) isEqual:(id)object{
    
    if (object == self) {
        return YES;
    }
    
    if (![object isKindOfClass:[self class]]) {
        return NO;
    }else{
        return [self.name isEqualToString: [object name]];
    }
}
-(NSUInteger)hash{
    return [self.name hash];
}















@end
