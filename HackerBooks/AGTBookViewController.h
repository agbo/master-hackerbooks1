//
//  AGTBookControllerViewController.h
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 17/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;

#import "AGTLibraryTableViewController.h"

@class AGTBook;

@interface AGTBookViewController : UIViewController<AGTLibraryTableViewControllerDelegate,UISplitViewControllerDelegate>

@property (strong, nonatomic) AGTBook *model;
@property (weak, nonatomic) IBOutlet UIImageView *coverView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *favoriteButton;

-(id) initWithModel:(AGTBook*) book;

- (IBAction)readBook:(id)sender;
- (IBAction)flipFavorite:(id)sender;

@end
