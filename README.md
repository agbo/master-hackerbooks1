#Práctica Resuelta de iOS Fundamentos KeepCoding

## HackerBooks

Un lector de libros en PDF para iPhone & iPad . Los libros en sí los recibirás en un json que deberás descargar de una URL.

## Modelo

Un libro contiene las siguientes propiedades: 
* título
* autores (una lista)
* tags (una lista)
* url a la imagen del libro
* url al pdf

Necesitarás una clase AGTBook para esto.

Una biblioteca representa un conjunto de libros. Una biblioteca permite acceder a los libros de varias maneras: por orden alfabético de título o por temática (los tags). Esto se debe de ver reflejado en una clase AGTLibrary. 


**Antes de seguir** piensa en cómo modelar esto. Intenta crear algunas clases que lo representen y que respondan a las necesidades de
una tabla.

Sigue con el ejercicio después de haber dedicado al menos algún tiempo para pensar en el modelo.


AGTLibrary debe de tener los siguientes métodos:

```Objective-C
// Número total de libros
-(NSUInteger) booksCount;

// Array inmutable (NSArray) con todas las 
// distintas temáticas (tags) en orden alfabético. 
// No puede bajo ningún concepto haber ninguna
// repetida.
-(NSArray*) tags;
 
// Cantidad de libros que hay en una temática. 
// Si el tag no existe, debe de devolver cero
-(NSUInteger) bookCountForTag:(NSString*) tag;

// Array inmutable (NSArray) de los libros 
// (instancias de AGTBook) que hay en 
// una temática. 
// Un libro puede estar en una o más 
// temáticas. Si no hay libros para una 
// temática, ha de devolver nil.
-(NSArray*) booksForTag: (NSString *) tag;


// Un AGTBook para el libro que está en la posición 
// 'index' de aquellos bajo un cierto
// tag. Mira a ver si puedes usar el método anterior 
// para hacer parte de tu trabajo.
// Si el indice no existe o el tag no existe, ha de devolver nil.
-(AGTBook*) bookForTag:(NSString*) tag atIndex:(NSUInteger) index;
```

###Procesado del JSON

Recibirás un JSON con un array de libros. Cada libro tendrá esta pinta:

```JSON
{
        "authors": "Scott Chacon, Ben Straub",
        "image_url": "http://hackershelf.com/media/cache/b4/24/b42409de128aa7f1c9abbbfa549914de.jpg",
        "pdf_url": "https://progit2.s3.amazonaws.com/en/2015-03-06-439c2/progit-en.376.pdf",
        "tags": "version control, git",
        "title": "Pro Git"
}
```


Deberás procesarlo para convertirlo en objetos de las clases AGTBook y AGTLibrary. Para averiguar como se convierte un JSON en un ```NSDictionary``` o un ```NSArray``` de ```NSDictionaries```, consulta la clase ```NSJSONSerialization```en la ayuda. Necesitas uno de sus métodos de clase. 

Ten en cuenta que dicho método devuelve un ```id``` que puede contener tanto un ```NSArray``` de ```NSDictionaries``` como un ```NSDictionary```. Mira en la ayuda en método ```isKindOfClass:``` y como usarlo para saber qué te han devuelto exactamente. ¿Qué otros métodos similares hay? ¿En qué se distingue ```isMemberOfClass:```?



## Especificaciones de HackerBooks

### Modelo
Al arrancar la App por primera vez (esto lo averiguas con ```NSUserDefaults```), descarga el JSON (tienes la URL en la sección de recursos) como un ```NSData``` y guárdalo en la carpeta Documents de tu Sandbox. Para ello, usa el método ```dataWithContentsOfURL:``` de ```NSData```. 

Haz lo mismo para las imágenes de portada y los pdfs. ¿Donde guardarías estos datos? Cambia las urls de remotas a locales, para que la próxima vez que se cargue la App, se pillen los datos locales.

Verás que todo esto tarda un poco y bloquea la interfaz de la aplicación durante un breve momento. No te preocupes por ello a estas alturas. En otra asignatura veremos como resolver esto de forma elegante.

En las siguientes ocasiones que arranque la App (ya sabemos que tenemos el JSON y los datos en local), crea tu modelo a partir de él. Crea un inicializador en AGTLibrary que recibe el JSON como un NSData y carga los datos de ahí.

### Tabla de libros
La biblioteca (```AGTLibrary```) se debe de mostrar en una tabla con celdas personalizadas. Piensa en qué datos deseas mostrar en dicha celda y cómo.  Necesitarás un controlador ```AGTLibraryTableViewController```.  

Dicha tabla debe de tener tantas secciones como distintos tags haya en tu biblioteca y una más, la de libros favoritos (Favorites). Las secciones deben de aparecer de la siguiente manera:
* primero Favorites
* luego todos los demás en orden alfabético.

El ser o no favorito se indicará mediante una propiedad booleana de AGTBook (```isFavorite```) y esto se debe de guardar en el sistema de ficheros y recuperar de alguna forma. Es decir, esa información debe de persistir de alguna manera cuando se cierra la App y cuando se abre.

¿Cómo harías eso? ¿Se te ocurre más de una forma de hacerlo? Explica la decisión de diseño que hayas tomado.

Cuando cambia el valor de la propiedad ```isFavorite``` de un AGTBook, la tabla deberá reflejar ese hecho. ¿Cómo lo harías? Es decir, ¿cómo enviarías información de un AGTBook a un AGTLibraryTableViewController? ¿Se te ocurre más de una forma de hacerlo? ¿Cual te parece mejor? Explica tu elección. 

**Nota:** para que la tabla se actualice, usa el método ```reloadData``` de ```UITableView```. Esto hace que la tabla vuelva a pedir datos a su dataSource. ¿Es esto una aberración desde el punto de rendimiento (volver a cargar datos que en su mayoría ya estaban correctos)? Explica por qué no es así. ¿Hay una forma alternativa? ¿Cuando crees que vale la pena usarlo?

### Controlador de libros

Crea un controlador que muestra la información de un libro:
* el título
* la portada en grande
* los tags 
* en general, todo excepto el pdf.

Debe de contar con algún elemento para marcarlo como favorito o no. Es decir, ahí es donde el usuario debe de poder marcar un libro como favorito o no.

También ha de contar con un botón para mostrar el pdf. El PDF se mostrará en otro controlador y tendrás que hacer un push.

Pon ambos controladores (el de tabla y el de libro en un ```UISPlitViewController```). Cuando el usuario selecciona un nuevo libro en la tabla, el controlador de libro debe de actualizarse. 

### Controlador de PDF: AGTSimplePDFViewController

Para mostrar un PDF, de momento usaremos una ```UIWebView``` y su método ```loadData:mimeType:textEncoding:```. Busca en la documentación o en StackOverflow cómo usarlo para cargar un PDF.

Cuando el usuario cambia en la tabla el libro seleccionado, el AGTSimplePDFViewController debe de actualizarse. ¿Cómo lo harías?


### App Universal

Haz la versión iOS del lector.

*** HASTA AQUI LLEGA LA PARTE BÁSICA Y OBLIGATORIA DE LA PRÁCTICA ***

Con esto repasamos los conceptos fundamentales del desarrollo iOS. No sigais hasta tener esto claro. Haced todas las preguntas que se os ocurran. 

Si estás viendo este material por primera vez, puedes parar aquí. Me interesa que tengas las ideas fundamentales muy claras. Prefiero que camines con absoluta seguridad a que corras dando tumbos y cayéndote a cada 3 pasos. 

Si ya lo has visto, es muy recomendable que sigas.

Para entregar la práctica, deja la versión final de la parte obligatoria en la rama _master_ de tu repositorio. Si deseas avanzar en la parte extra, hazlo en la rama _extra_.

### PARTE EXTRA: Mostrar PDFs como Jobs manda

El mundo es de los valientes y parar aquí sería como si [Chuck Wepner](http://www.hossli.com/articles/2007/01/01/chuck-wepner-–-the-real-rocky/) hubiese tirado la toalla en el primer asalto. Si quereis hacer como él, [tumbar al campeón del mundo](https://www.youtube.com/watch?v=X_92K8k31rU) y llegar al décimo quinto asalto, tomaos un copazo de orujo, poned cara de Chuck Norris y seguid leyendo. Los demás, tomaos un Petit Suisse de fresa.

Usar un ```UIWebView``` para mostrar un PDF es una barbaridad y además por los varios bugs que tiene esa vista, estareis filtrando memoria. Hay muchas librerías de terceros para manejar PDFs. Una buena y gratuita es [vfrReader](https://github.com/vfr/Reader).

a) Averigua qué es [CocoaPods](http://cocoapods.org) y cómo se usa, leyendo la documentación en el sitio de CocoaPods. También puedes usar un submódulo de Git si lo prefieres.

b) Instala la vfrReader y crea un nuevo controlador AGTPDFViewController y úsalo para mostrar los libros.

### Extras

a) ¿Qué funcionalidades le añadirías antes de subirla a la App Store?

b) Ponle otro nombre e intenta subir esta primera versión a la App Store. Como aun no tienes idea de diseño, [aquí tienes plantillas gratis y de pago](http://www.appdesignvault.com/portfolio-condensed/)

c) Usando esta App como "plantilla", ¿qué otras versiones se te ocurren? ¿Algo que puedas monetizar?

Es vital que vayas teniendo un portfolio de Apps y código para mostrarlo a potenciales empleadores, así que saca tu App del armario y súbela a la App Store.


## Forma de Entrega

Crea un repo con la App e invítame a él (mi usuario en bitBucket y gitHub es el mismo: frr149). 

Debes de incluir tanto el proyecto como un fichero llamado README.md (lo de md significa [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)) donde debes de responder a las preguntas que se hacen en este documento así como añadir cualquier comentario que consideres oportuno.


## Recursos

Enlace al [JSON](https://t.co/K9ziV0z3SJ) 

## Respuestas a preguntas que probablemente serán frecuentes

* ¿Puedo entregar sólo la parte obligatoria? 
    - _Efectivamente, joven padawan... ;-)_
* ¿Puedo entregar más tarde la parte _extra_ para que sea corregida y evaluada?
    - Sin duda, de eso se trata. Es muy recomendable que lo hagas. Basta con que actualices tu repo y me avises.
* ¿Hasta cuando puedo entregar la parte extra?
    - Hasta que termine las clases del Master y antes que empiece la mega práctica. Eso sí, quien lo entregue antes de empezar iOS Avanzado le invitaré a una caña en la primera oportunidad. Además entre los que la entreguen completa al final de la semana de asimilación, sortearé un genuino bolígrafo de la manzana que he comprado en la tienda de Apple en Cupertino hace dos semanas. Se rumorea que era el que usaba Steve Jobs para firmar los [despidos en el ascensor](http://www.quora.com/Who-did-Steve-Jobs-actually-fire-in-an-elevator). ¿Quieres tener en tus manos un pedazo de la historia del Silicon Valley? Pues ponte a trabajar YA.
