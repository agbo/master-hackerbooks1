//
//  AGTBookSectionTableViewCell.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 14/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTBookSectionTableViewCell.h"

@implementation AGTBookSectionTableViewCell

+(CGFloat) height{
    return 24;
}

+(NSString*) cellId{
    return NSStringFromClass(self);
}

- (void)awakeFromNib {
    // Initialization code
    
    
}



@end
