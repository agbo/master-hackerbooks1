//
//  AGTBookTagTests.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 12/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "AGTBookTag.h"

@interface AGTBookTagTests : XCTestCase

@end

@implementation AGTBookTagTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


-(void) testNameNormalization{
    
    AGTBookTag *tag = [AGTBookTag bookTagWithName:@"lINEar aLGebra"];
    AGTBookTag *emptyTag = [AGTBookTag bookTagWithName:@""];
    AGTBookTag *singleTag = [AGTBookTag bookTagWithName:@"a"];
    
    XCTAssertEqualObjects(@"Linear algebra", [tag normalizedName]);
    XCTAssertEqualObjects(@"", [emptyTag normalizedName]);
    XCTAssertEqualObjects(@"A", [singleTag normalizedName]);
    
}

-(void) testOrdering{
    
    AGTBookTag *fav = [AGTBookTag bookTagWithName:@"faVorite"];
    AGTBookTag *other = [AGTBookTag bookTagWithName:@"shanKar Ali khan"];
    AGTBookTag *a1 = [AGTBookTag bookTagWithName:@"A1A Car Wash"];
    
    XCTAssertEqual(NSOrderedSame, [fav compare:fav], @"Should be equal");
    XCTAssertEqual(NSOrderedAscending, [fav compare:a1], @"Favorite should always be smaller");
    XCTAssertEqual(NSOrderedDescending, [other compare:a1], @"Shankar should always be bigger");
    XCTAssertEqual(NSOrderedDescending, [other compare:fav],@"Favorite should always be smaller");

}

-(void) testSort{
    
    AGTBookTag *design = [AGTBookTag bookTagWithName:@"design"];
    AGTBookTag *data = [AGTBookTag bookTagWithName:@"data visualization"];
    AGTBookTag *fav = [AGTBookTag bookTagWithName:@"favorite"];
    
    
    NSArray *sorted = @[fav, data, design];
    NSArray *toBeSorted = [@[data, fav, design] sortedArrayUsingSelector:@selector(compare:) ];
    XCTAssertEqualObjects(sorted, toBeSorted);
}
-(void)testEquality{
    
    AGTBookTag *a = [AGTBookTag bookTagWithName:@"AaaaA"];
    AGTBookTag *a1 = [AGTBookTag bookTagWithName:@"AaAaA"];
    AGTBookTag *b = [AGTBookTag bookTagWithName:@"crash"];
    
    XCTAssertEqualObjects(a, a);
    XCTAssertEqualObjects(a, a1);
    
    XCTAssertNotEqualObjects(a, b);
    XCTAssertNotEqualObjects(a, @"b");
    
    
}

-(void) testFavoriteTag{
    AGTBookTag *a = [AGTBookTag bookTagWithName:@"AaaaA"];
    AGTBookTag *fav =[AGTBookTag favoriteBookTag];
    
    XCTAssertTrue([fav isFavorite]);
    XCTAssertFalse([a isFavorite]);
    
}

@end
