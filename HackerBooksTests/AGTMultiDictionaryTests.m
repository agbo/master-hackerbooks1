//
//  AGTMultiDictionaryTests.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 11/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "AGTMultiDictionary.h"


@interface AGTMultiDictionaryTests : XCTestCase

@end

@implementation AGTMultiDictionaryTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


-(void) testThatAddingAnObjectToEmptyKeyReturnsNSSetWithObject{
    
    AGTMultiDictionary *dict = [AGTMultiDictionary new];
    [dict addObject:@"hedgehog" forKey:@"sonic"];
    
    id objs = [dict objectsForKey:@"sonic"];
    XCTAssert([objs isKindOfClass:[NSSet class]]);
    XCTAssert([objs count] == 1);
    XCTAssertNotNil([objs member:@"hedgehog"]);
}

-(void) testThatObjectsGetAddedToExistingBuckets{
    
    AGTMultiDictionary *dict = [AGTMultiDictionary new];
    [dict addObject:@"hedgehog" forKey:@"sonic"];
    
    [dict addObject:@42 forKey:@"sonic"];
    
    XCTAssertNotNil([[dict objectsForKey:@"sonic"] member:@42]);
    
}

-(void) testThatAddingAnExistingObjectToAnotherBucketDoesntChangeCount{
    
    AGTMultiDictionary *dict = [AGTMultiDictionary new];
    
    [dict addObject:@"hedgehog" forKey:@"animal"];
    [dict addObject:@"hedgehog" forKey:@"videogameCharacter"];
    
    XCTAssertEqual([dict count], 1,
                   @"its the same object in 2 different buckets");
                   
}

-(void) testThatRemovingAnObjectFromABucketWhileItsStillInAnotherDoesntChangeCount{
    
    AGTMultiDictionary *dict = [AGTMultiDictionary new];
    
    [dict addObject:@"hedgehog" forKey:@"animal"];
    [dict addObject:@"hedgehog" forKey:@"videogameCharacter"];
    [dict addObject:@"hedgehog" forKey:@"mammal"];
    
    [dict removeObject:@"hedgehog"
                forKey:@"videogameCharacter"];
    
    XCTAssertEqual([dict count], 1, @"The object is still there");

}

-(void) testCountComputesObjectsInDifferentBuckets{
    
    AGTMultiDictionary *dict = [AGTMultiDictionary new];
    
    [dict addObject:@"shark" forKey:@"animal"];
    [dict addObject:@"hedgehog" forKey:@"videogameCharacter"];
    [dict addObject:@"hedgehog" forKey:@"mammal"];
    
    XCTAssertEqual([dict count], 2);
}
-(void) testThatAddingSameObjectSeveralTimesToSameBucketDoesntChangeCount{
    
    AGTMultiDictionary *dict = [AGTMultiDictionary new];
    
    [dict addObject:@"hedgehog" forKey:@"animal"];
    [dict addObject:@"hedgehog" forKey:@"animal"];
    [dict addObject:@"hedgehog" forKey:@"animal"];
    [dict addObject:@"hedgehog" forKey:@"animal"];
    
    XCTAssertEqual([dict count], 1, @"It's always teh same");
    XCTAssertEqual([[dict objectsForKey:@"animal"] count], 1,
                   @"Should never be added more than once");
    
}

-(void) testIfAnObjectIsIn2BucketsAndRemovedFromOneStillExistsInTheOther{
    
    AGTMultiDictionary *dict = [AGTMultiDictionary new];
    
    [dict addObject:@"hedgehog" forKey:@"animal"];
    [dict addObject:@"hedgehog" forKey:@"mammal"];
    
    [dict removeObject:@"hedgehog"
                forKey:@"mammal"];
    
    XCTAssertEqual([dict count], 1);
    XCTAssertEqual([[dict objectsForKey:@"animal"] count], 1);
    XCTAssertEqualObjects([[[dict objectsForKey:@"animal"] allObjects]lastObject],
                          @"hedgehog");

}

-(void) testAddingAndRemovingBuckets{
    
    AGTMultiDictionary *dict = [AGTMultiDictionary new];
    
    XCTAssertEqual(dict.count, 0, @"Should be empty");
    
    [dict addObject:@"yo"
             forKey:@"breakingBadVocabulary"];
    
    XCTAssertEqual(dict.count, 1, @"Should be 1");
    
    [dict removeObject:@"yo"
                forKey:@"breakingBadVocabulary"];
    
    XCTAssertEqual(dict.count, 0, @"Empty bucket should have been removed");
    
}
@end
