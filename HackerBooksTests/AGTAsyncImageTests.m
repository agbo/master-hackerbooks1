//
//  AGTAsyncImageTests.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 15/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "AGTAsyncImage.h"

@interface AGTAsyncImageTests : XCTestCase
@property (strong, nonatomic) AGTAsyncImage *aImage;
@property (nonatomic, strong) NSBundle *testBundle;
@property (nonatomic, strong) UIImage *defaultImage;
@property (nonatomic, strong) NSURL *localURL;


@end

@implementation AGTAsyncImageTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    self.testBundle = [NSBundle bundleForClass:[self class]];
    self.localURL =[self.testBundle URLForResource:@"emptyBookCover"
                                     withExtension:@"png"];
    self.defaultImage = [UIImage imageWithData:
                         [NSData dataWithContentsOfURL:self.localURL]];
                          
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    self.testBundle = nil;
    self.localURL = nil;
    self.defaultImage = nil;
    
}



-(void) testThatPassingURLWithoutFileNameReturnsNil{
    
    NSURL *invalidURL = [NSURL URLWithString:@"http://www.google.com/"];
    NSURL *invalidURL2 = [NSURL URLWithString:@"http://www.google.com"];
    NSURL *validURL = [NSURL URLWithString:@"http://www.google.com/a.jpg"];
    
    
    XCTAssertNil([AGTAsyncImage asyncImageWithURL:invalidURL
                                        defaultImage: self.defaultImage], @"Invalid Input");
    XCTAssertNil([AGTAsyncImage asyncImageWithURL:invalidURL2
                                        defaultImage: self.defaultImage], @"Invalid Input");
    XCTAssertNotNil([AGTAsyncImage asyncImageWithURL:validURL
                                        defaultImage: self.defaultImage], @"Invalid Input");
}

@end
