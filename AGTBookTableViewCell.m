//
//  AGTBookTableViewCell.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 14/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#define FAVORITE_KEY @"isFavorite"

#import "AGTBookTableViewCell.h"
#import "AGTBook.h"


@interface AGTBookTableViewCell()
@property (nonatomic, strong) AGTBook *model;

@end
@implementation AGTBookTableViewCell

#pragma mark -  Class Methods
+(CGFloat) height{
    return 94;
}

+(NSString *)cellId{
    return NSStringFromClass(self);
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)flipFavoriteState:(id)sender {
    
    self.model.isFavorite = !self.model.isFavorite;
}

#pragma mark -  Notificaciones
-(void) observeBook:(AGTBook*) book{
    
    self.model = book;
    
    [self addObserver];
    
    [self syncWithBook];
    
    
    
}

-(void) addObserver{
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    // Muy importante, solo nos interesa los cambios en NUESTRO libro!
    [nc addObserver:self
           selector:@selector(syncWithBook)
               name:AGTBOOK_DID_CHANGE_NOTIFICATION
             object:self.model];
}

-(void) removeObserver{
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self];

}


-(void) dealloc{
    
    // baja en notificaciones
    [self removeObserver];
}



#pragma mark -  Sync
-(void) syncWithBook{
    
    // Puede cambiar imagen y favoritos
    [UIView transitionWithView:self.coverView
                      duration:0.7
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.coverView.image = self.model.coverImage;
                    } completion:nil];
    
    
    [self syncFavoriteState];
}

-(void)syncFavoriteState{
    
    UIColor *defaultColor = [UIColor colorWithWhite:0.69
                                                     alpha:1];
    UIColor *favColor = [UIColor colorWithRed:246.0/255.0
                                        green:162.0/255.0
                                         blue:26.0/255.0
                                        alpha:1];
    
    if (self.model.isFavorite) {
        [self.favoriteButton setTitleColor:favColor
                                  forState:UIControlStateNormal];
    }else{
        [self.favoriteButton setTitleColor:defaultColor
                                  forState:UIControlStateNormal];
    }
}




#pragma mark - Cleanup
-(void) prepareForReuse{
    [super prepareForReuse];
    
    
    // hacemos limpieza
    [self cleanUp];
}

-(void) cleanUp{
    
    // baja en notificaciones
    [self removeObserver];

    self.model = nil;
    self.coverView.image = nil;
    self.titleView.text = nil;
    self.authorsView.text = nil;
    self.tagsView.text = nil;
}


@end
