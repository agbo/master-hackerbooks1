//
//  AGTBookTableViewCell.h
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 14/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;

@class AGTBook;

@interface AGTBookTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *coverView;
@property (weak, nonatomic) IBOutlet UILabel *titleView;
@property (weak, nonatomic) IBOutlet UILabel *authorsView;
@property (weak, nonatomic) IBOutlet UILabel *tagsView;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;


+(CGFloat) height;
+(NSString *)cellId;

- (IBAction)flipFavoriteState:(id)sender;

-(void) observeBook:(AGTBook*) book;

-(void) cleanUp;

@end
